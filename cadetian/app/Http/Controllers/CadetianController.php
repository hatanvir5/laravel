<?php

namespace App\Http\Controllers;

use App\Cadetian;
use Illuminate\Http\Request;

class CadetianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
       
    }
    // public function tanvir()
    // {
    //     $cadetian=Cadetian::all();
    //  return view('layouts.Cadetian.view',compact('cadetian'));
    // }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.Cadetian.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $cadetian=new Cadetian;
        $cadetian->name=$request->name;
        $cadetian->email=$request->email;
        $cadetian->address=$request->address;
        $cadetian->mobile=$request->mobile;
        $cadetian->bloodGroup=$request->bloodGroup;
        $cadetian->save();
     return redirect(route('home'));
    

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cadetian  $cadetian
     * @return \Illuminate\Http\Response
     */
    public function show(Cadetian $cadetian)
    {
        $cadetian=Cadetian::paginate(5);
        return view('layouts.Cadetian.view',compact('cadetian'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cadetian  $cadetian
     * @return \Illuminate\Http\Response
     */
    public function edit(Cadetian $cadetian, $id)
    {
        $cadetian=Cadetian::find($id);
        return view('layouts.Cadetian.edit',compact('cadetian'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cadetian  $cadetian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Cadetian $cadetian)
    {
        $cadetian=Cadetian::find($id);
        $cadetian->name=$request->name;
        $cadetian->email=$request->email;
        $cadetian->address=$request->address;
        $cadetian->mobile=$request->mobile;
        $cadetian->bloodGroup=$request->bloodGroup;
        $cadetian->save();
     return redirect(route('home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cadetian  $cadetian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cadetian $cadetian,$id)
    {
        $cadetian=Cadetian::find($id);
        $cadetian->delete();
        return redirect(route('home'));
    }
}
