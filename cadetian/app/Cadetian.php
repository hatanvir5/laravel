<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cadetian extends Model
{
    protected $fillable=['name','email','address','bloodGroup','mobile'];
}