<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <title>Document</title>
</head>
<body class="container">
<h2 class="text-center mt-4 pt-2 pb-2 bg-dark text-light">Cadetian Information</h2>
    <table class="table table-striped table-dark">
        <thead>
          <tr>
            {{-- <th scope="col">ID</th> --}}
            <th scope="col"  class="text-center">Name</th>
            <th scope="col"  class="text-center">Address</th>
            <th scope="col"  class="text-center">Mobile</th>
            <th scope="col"  class="text-center"> Blood Group</th>
            <th scope="col"  class="text-center">Email</th>
            <th scope="col" class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
            <?php
    
    
            foreach ($cadetian as $key => $singleCadet) {
        $id=$singleCadet['id'];
        $name=$singleCadet->name;
        $email=$singleCadet->email;
        $mobile=$singleCadet['mobile'];
        $address=$singleCadet['address'];
        $bloodGroup=$singleCadet['bloodGroup'];
              
   ?>
          <tr>
            
         
            <td  class="text-center"><?= $name?></td>
            <td  class="text-center"><?= $address?></td>
            <td  class="text-center"><?= $mobile?></td>
            <td  class="text-center"><?= $bloodGroup?></td>
            <td  class="text-center"><?= $email?></td>
            <td class="text-center">
            <a class="btn btn-primary" href="{{route('edit',$id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a>
            <form action="{{route('delete',$id)}}" method="post" style="display: none" id="delete-{{$id}}">
              {{ csrf_field() }}
              {{method_field('delete')}}
            </form>
            <button onclick="if(confirm('are you sure , you want to delete this? ')){
              event.preventDefault;
            document.getElementById('delete-{{$id}}').submit();
            }
            else{
              event.preventDefault;
            }
            
            " class="btn btn-danger" type="submit"><i class="fa fa-trash" aria-hidden="true"></i> </button>
           
              
                </td>
           
          </tr>
          <?php
        }
          ?>

        </tbody>
      </table>

      <a class="btn btn-primary mb-4" href="{{route('create')}}">Add New</a>

      {{$cadetian->links()}}

      <script src="https://use.fontawesome.com/165197dbd9.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>


