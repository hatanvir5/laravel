<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
 
    
<div class="container row">


    <div class="col-5"></div>
    <div class="col-6">
    <h3 class="mt-5">Cadetian Registration</h3>

    <form class="form-horizontal" action="{{route('store')}}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
              <label>Name</label>
              <input type="text" class="form-control"  name="name">
            </div>
          
            <div class="form-group">
                <label for="exampleFormControlInput1">Email:</label>
                <input type="email" class="form-control" id="exampleFormControlInput1" name="email">
              </div>
              
            <div class="form-group">
                <label for="exampleFormControlInput1"><Address>Address:</Address></label>
                <input type="text" class="form-control"  name="address">
              </div>
              
            <div class="form-group">
                <label for="exampleFormControlInput1">Mobile:</label>
                <input type="tel" class="form-control" id="exampleFormControlInput1" name="mobile">
              </div>
              
            <div class="form-group">
                <label for="bloodGroup">Blood Group:</label>
                    <select name="bloodGroup" class="form-control" >
                        <option value="A+">A+</option>
                        <option value="A-">A-</option>
                        <option value="AB+">AB+</option>
                        <option value="AB-">AB-</option>
                        <option value="O+">O+</option>
                        <option value="O-">O-</option>
                        <option value="B+">B+</option>
                        <option value="B-">B-</option>
                    </select>
              </div>
              <button class="btn btn-primary btn-large btn-block" type="submit">Save</button>
          </form>
          
    </div>
    <div class="col-3"> </div>
</div>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>