<?php

use Illuminate\Database\Seeder;

class CadetianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     factory(   App\Cadetian::class)->create();
    }
}
