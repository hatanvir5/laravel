<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cadetian;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Cadetian::class, function (Faker $faker) {
    return [
        'name'       => $faker->name,
        'email'       => $faker->sentence,
        'address'       => $faker->sentence,
        'bloodGroup'       => $faker->word,
        'mobile'       => $faker->randomNumber,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
