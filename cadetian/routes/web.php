<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tanvir', 'CadetianController@show')->name('home');
Route::get('/create', 'CadetianController@create')->name('create');
Route::Post('/create', 'CadetianController@store')->name('store');
Route::get('/edit{id}', 'CadetianController@edit')->name('edit');
Route::post('/update{id}', 'CadetianController@update')->name('update');
Route::delete('/delete{id}', 'CadetianController@destroy')->name('delete');
