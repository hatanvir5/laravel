<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\product;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(product::class, function (Faker $faker) {
    return [
        'title'=> $faker->word,
        'sku'=>$faker->word,
        'description'=>$faker->paragraph,
        'price'=>$faker->randomFloat,
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
