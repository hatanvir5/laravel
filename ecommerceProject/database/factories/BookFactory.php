<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'author' => $faker->sentence,
        'price'=>$faker->randomNumber,       
      
    ];
});
