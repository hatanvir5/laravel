<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('sku');
            $table->string('title');
            $table->string('short_description')->nullable();
            $table->string('description')->nullable();
            $table->string('url_key')->nullable();

            $table->decimal('cost', 12, 4)->nullable();
            $table->decimal('price', 12, 4)->nullable();
            $table->boolean('special_price')->nullable();
            $table->date('special_price_from')->nullable();
            $table->date('special_price_to')->nullable();

            $table->integer('stock')->nullable();

            $table->decimal('weight', 12, 4)->nullable();
            $table->integer('color')->nullable();
            $table->integer('size')->nullable();

            $table->boolean('is_new')->nullable();
            $table->boolean('is_featured')->nullable();
            $table->boolean('is_published')->nullable();
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
