<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\product;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(product::class, function (Faker $faker) {
    return [
        'sku'       => $faker->word,
        'title'       => $faker->word,
        'short_description'=> $faker->paragraph,
        'description'  => $faker->paragraph,
        'cost'       => $faker->word,
        'price'       => $faker->randomFloat,
        'stock'       => $faker->boolian,
        'weight'       => $faker->randomInteger,
        
        'created_at' => Carbon::now()->toDateTimeString(),
        'updated_at' => Carbon::now()->toDateTimeString(),
    ];
});
